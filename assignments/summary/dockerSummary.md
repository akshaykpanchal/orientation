**What is Docker?**

_An open-source project that automates the deployment of software applications inside containers by providing an additional layer of abstraction and automation of OS-level virtualization on Linux_.

**Why Docker?**

_For developers, there arises a situation where they need to work on different projects. And for each different project they might need a specific operating system, or a specific version of operating system. This becomes a tedious job to do. This problem can be solved using virtual machines, but this also requires exact enviornment to run an application. Here comes the concept of Docker. The developer can work on the application and save it as docker image and create a docker container. This container has everything which is required to run an application alongwith the enviornment. This docker is then stored to repository and then can be used by other developers. So docker is used by many developers._


**Docker Terminologies**

* _Docker_
* _Docker Image_
* _Container_
* _Docker Hub_

_Docker is a program for developers to develop, and run applications with containers.A Docker image is contains everything needed to run an applications as a container. This includes:code,runtime,libraries,environment variables,configuration files.A Docker container is a running Docker image.From one image you can create multiple containers.Docker Hub is like GitHub but for docker images and containers_.

![](doc.png)

**Docker Components**

* Docker Engine:
The place where containers and runs the programs.
* Docker Client:
It is the end user which provides the demands of the docker file as commands.
* Docker daemon:
This place checks the client request and communicate with docker components such as image, container, to perform the process.
* Docker Registry:
A place docker images are stored. DockerHub is such a public registry.

![](dc.png)

**Docker basic commands**

```
 docker ps :_The docker ps command allows us to view all the containers that are running on the Docker Host._
 docker start:_This command starts any stopped container(s)._
 docker stop:_This command stops any running container(s)_
 docker run:_This command creates containers from docker images._
 docker rm:_This command deletes the containers_
```


**Operations on Dockers**

* Download the docker.
``` docker pull snehabhapkar/trydock```
* Run the docker image with this command
``` docker run -ti snehabhapkar/trydock /bin/bash```
* Copy file inside the docker container
``` docker cp hello.py e0b72ff850f8:/```
* All write a script for installing dependencies - requirements.sh
``` apt update```
``` apt install python3```
