**GIT**

_Git is a version control (VCS) system for tracking changes to projects_.
_Version control systems are also called revision control systems or source code management (SCM) systems_.

_Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows_.


![](vcs.png)


**What’s the point of Git?**

_Suppose you’re working on a group assignment with some friends, and everyone has their own brilliant ideas. Consider two ways to combine these ideas together:_

* _Person A writes his/her/their ideas on a single sheet of paper and passes it on to Person B, who writes his/her/their ideas on the paper and passes it on to Person C, etc._
* _Everyone writes their ideas down on separate pieces of paper, and then consolidates them into a single sheet afterward._


**Using Git**

_Before starting to use Git you should first tell Git who you are so that you can be identified when you make contributions to projects. This can be done by specifying your name and email address_.

```
git config --global user.name "Your Name Comes Here"

git config --global user.email your_email@yourdomain.example.com

```

**Starting a Project with Git**

_You can create a directory for your new project using the mkdir command_.

``` 
mkdir myproject
```

_or you can work with a a project directory that already exists. You can enter that directory with_

```
cd myproject
```



**Summary of Essential Git Commands**

```
git status: check status and see what has changed

git add: add a changed file or a new file to be committed

git diff: see the changes between the current version of a file and the version of the file t recently committed

git commit: commit changes to the history

git log: show the history for a project

git revert: undo a change introduced by a specific commit

git checkout: switch branches or move within a branch

git clone: clone a remote repository

git pull: pull changes from a remote repoository

git push: push changes to remote repository
```

**Git workflow**
![](gw.jpeg)

![](gw2.jpg)
